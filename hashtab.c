/*
 * Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "hashtab.h"


#define KV_USED		(1 << 0)


struct hashtab_kv {
	void *key;
	void *val;
	size_t key_size;
	size_t val_size;
	uint8_t flags;
};

static uint32_t
pjw_hash(void *key, size_t size)
{
	int i;
	uint8_t *ptr = (uint8_t *) key;
	uint32_t h = 0, hi;

	for (i = 0; i < size; i++) {
		h = (h << 4) + ptr[i];
		if ((hi = h & 0xf0000000))
			h ^= hi >> 24;
		hi &= ~hi;
	}
	return h;
}

static int
alloc(struct hashtab *ht, void *key, size_t size, size_t *idx)
{
	size_t i = 0;
	size_t h;

	h = ht->hash(key, size) % ht->size;
	while (i++ < ht->size) {
		if (!(ht->kv[h].flags & KV_USED)) {	/* 1. empty slot */
			*idx = h;
			return 1;
		}
		if (ht->kv[h].key_size == size) {	/* 2. used by the same
							 * key */
			if (!(memcmp(ht->kv[h].key, key, size))) {
				*idx = h;
				return 1;
			}
		}
		h = (h + 1) % ht->size;	/* 3. used by a different key */
	}
	return 0;
}

static int
lookup(struct hashtab *ht, void *key, size_t size, size_t *idx)
{
	size_t i = 0;
	size_t h;

	h = ht->hash(key, size) % ht->size;
	while (i++ < ht->size) {
		if (ht->kv[h].key_size == size) {
			if (!(memcmp(ht->kv[h].key, key, size))) {
				*idx = h;
				return 1;
			}
		}
		h = (h + 1) % ht->size;
	}
	return 0;
}

static void
put(struct hashtab *ht, size_t idx, void *key, size_t ksize, void *val,
    size_t vsize)
{
	ht->kv[idx].key = malloc(ksize);
	ht->kv[idx].key_size = ksize;
	ht->kv[idx].val = malloc(vsize);
	ht->kv[idx].val_size = vsize;
	memcpy(ht->kv[idx].key, key, ksize);
	memcpy(ht->kv[idx].val, val, vsize);
	ht->kv[idx].flags |= KV_USED;
}

static void
get(struct hashtab *ht, size_t idx, void **val, size_t *size)
{
	*val = ht->kv[idx].val;
	*size = ht->kv[idx].val_size;
}

static void
del(struct hashtab *ht, size_t idx)
{
	free(ht->kv[idx].key);
	free(ht->kv[idx].val);
	memset(&ht->kv[idx], 0, sizeof(struct hashtab_kv));
}

int
hashtab_init(struct hashtab *ht, uint8_t size, struct hashtab_options *opts)
{
	if (ht == NULL)
		return 0;
	if (size < HTMIN || size > HTMAX)
		return 0;
	ht->size = 1UL << size;
	ht->kv = (struct hashtab_kv *) calloc(ht->size,
	    sizeof(struct hashtab_kv));
	ht->hash = pjw_hash;
	return 1;
}

int
hashtab_first(struct hashtab *ht, size_t *iter)
{
	if (ht == NULL || iter == NULL)
		return 0;
	for (*iter = 0; *iter < ht->size; (*iter)++) {
		if (!(ht->kv[*iter].flags & KV_USED))
			continue;
		return 1;
	}
	return 0;
}

int
hashtab_next(struct hashtab *ht, size_t *iter)
{
	if (ht == NULL || iter == NULL)
		return 0;
	for ((*iter)++; *iter < ht->size; (*iter)++) {
		if (!(ht->kv[*iter].flags & KV_USED))
			continue;
		return 1;
	}
	return 0;
}

int
hashtab_at(struct hashtab *ht, size_t iter, void **key, size_t *ksize,
    void **val, size_t *vsize)
{
	if (ht == NULL || key == NULL || ksize == NULL || val == NULL ||
	    vsize == NULL)
		return 0;
	if (iter >= ht->size)
		return 0;
	if (!(ht->kv[iter].flags & KV_USED))
		return 0;
	*key = ht->kv[iter].key;
	*ksize = ht->kv[iter].key_size;
	*val = ht->kv[iter].val;
	*vsize = ht->kv[iter].val_size;
	return 1;
}

int
hashtab_put(struct hashtab *ht, void *key, size_t ksize, void *val,
    size_t vsize)
{
	size_t idx;

	if (ht == NULL)
		return 0;
	if (!alloc(ht, key, ksize, &idx))
		return 0;
	put(ht, idx, key, ksize, val, vsize);
	return 1;
}

int
hashtab_get(struct hashtab *ht, void *key, size_t ksize, void **val,
    size_t *vsize)
{
	size_t idx;

	if (ht == NULL || val == NULL || vsize == NULL)
		return 0;
	if (!lookup(ht, key, ksize, &idx))
		return 0;
	get(ht, idx, val, vsize);
	return 1;
}

int
hashtab_del(struct hashtab *ht, void *key, size_t size)
{
	size_t idx;

	if (ht == NULL)
		return 0;
	if (!lookup(ht, key, size, &idx))
		return 0;
	del(ht, idx);
	return 1;
}
