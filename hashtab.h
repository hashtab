/*
 * Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef HASHTAB_H
#define HASHTAB_H

#include <stdint.h>

#define HTMIN 4
#define HTMAX 24

struct hashtab {
	struct hashtab_kv *kv;
	size_t size;
	uint32_t (*hash) (void *, size_t);
};

struct hashtab_options;

int hashtab_init(struct hashtab *, uint8_t, struct hashtab_options *);
int hashtab_put(struct hashtab *, void *, size_t, void *, size_t);
int hashtab_get(struct hashtab *, void *, size_t, void **, size_t *);
int hashtab_del(struct hashtab *, void *, size_t);
int hashtab_first(struct hashtab *, size_t *);
int hashtab_next(struct hashtab *, size_t *);
int hashtab_at(struct hashtab *, size_t, void **, size_t *, void **, size_t *);

#define HASHTAB_FOREACH(ht, itr, key, ksize, val, vsize)		\
	for (hashtab_first(&(ht), &(itr)) ;				\
		hashtab_at(&(ht), (itr), (void *)&(key), &(ksize),	\
			(void *)&(val), &(vsize))  ;			\
		hashtab_next(&(ht), &(itr))				\
	)

#endif
