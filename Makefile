LIB=		hashtab
SRCS=		hashtab.c
HDRS=		hashtab.h
SHLIB_MAJOR=	0
SHLIB_MINOR=	1
#MAN=		hashtab.3
#MLINKS=		hashtab.3 hashtab_init.3 \
#		hashtab.3 hashtab_put.3 \
#		hashtab.3 hashtab_get.3 \
#		hashtab.3 hashtab_del.3 \
#		hashtab.3 hashtab_first.3 \
#		hashtab.3 hashtab_next.3 \
#		hashtab.3 hashtab_at.3

CFLAGS+=	-Wall -Werror
COPTS+=		-g


includes:
	@cd ${.CURDIR}; for i in $(HDRS); do \
	    j="cmp -s $$i ${DESTDIR}/usr/include/$$i || \
		${INSTALL} ${INSTALL_COPY} -o ${BINOWN} -g ${BINGRP} \
		-m 444 $$i ${DESTDIR}/usr/include"; \
	    echo $$j; \
	    eval "$$j"; \
	done

.include <bsd.lib.mk>
