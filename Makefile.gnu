CFLAGS+= -Wall -I. -fPIC -g
LDFLAGS+= -L. -lbsd
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
all: hashtab.a hashtab.so example
%.o: %.c 
	$(CC) -o $@ -c $< $(CFLAGS)
hashtab.a: $(OBJS)
	ar cq hashtab.a hashtab.o
hashtab.so: $(OBJS)
	$(CC) -shared -o hashtab.so hashtab.o
example: hashtab.a
	$(CC) example.o -o example -l:hashtab.a $(LDFLAGS)
install: hashtab.a hashtab.so
	install -m 0644 hashtab.a /usr/lib/libhashtab.a
	install -m 0644 hashtab.so /usr/lib/libhashtab.so
	install -m 0644 hashtab.h /usr/include/
clean:
	rm -f *.o *.a *.so example
